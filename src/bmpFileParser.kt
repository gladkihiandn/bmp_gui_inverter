import com.sun.javafx.image.IntToBytePixelConverter
import java.io.File
import kotlin.experimental.inv

class bmpFileParser(file: File) {

    fun bytesToInt(bytesArray:ByteArray): Int {
        var result = 0
        bytesArray.forEachIndexed{i, byte-> result += byte.toInt().shl(i * 8)}
        return result
    }

    // Data for reading BMP file format
    val fileBytes: ByteArray = file.readBytes()
    val bfType = fileBytes.copyOfRange(0,2)
    val bfSize = bytesToInt(fileBytes.copyOfRange(2,6))
    val bfOffBits = bytesToInt(fileBytes.copyOfRange(10,15))

    var bdBMP = fileBytes.copyOfRange(bfOffBits, fileBytes.size)

    fun invertBMP(){
        if (!bdBMP.isEmpty()){
            bdBMP.forEachIndexed {i, byte ->
                bdBMP[i] = byte.inv()
            }
        }
    }

    fun saveNewBMP(fileName:String){
        val newBMP = File(fileName)
        newBMP.writeBytes(fileBytes.copyOfRange(0, bfOffBits))
        newBMP.appendBytes(bdBMP)
    }
}