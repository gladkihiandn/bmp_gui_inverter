import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.event.*;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

public class bmpParserGUI extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JButton openBMPFileButton;
    private JLabel inputFileName;
    private String defaultFileNameLabel = "FileName: ";
    private JLabel labelOutputFileName;
    private JTextField textOutputFileNameField;

    private bmpFileParser bmpParser;

    private String currentPath = "";

    public bmpParserGUI() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        openBMPFileButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOpenFile();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
     }

    private void onOpenFile() {
        JFileChooser fileopen = new JFileChooser(currentPath.toString());
        fileopen.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                } else {
                    return f.getName().toLowerCase().endsWith(".bmp");
                }
            }

            @Override
            public String getDescription() {
                return "BMP Image File (*.BMP)";
            }
        });
        int ret = fileopen.showDialog(null, "Открыть файл");
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = fileopen.getSelectedFile();
            currentPath = file.getParent();
            inputFileName.setText(defaultFileNameLabel + file.getName());
            textOutputFileNameField.setEnabled(true);

            bmpParser = new bmpFileParser(file);
        }
    }

    private void onOK() {
        if (textOutputFileNameField.isValid())
        {
            bmpParser.invertBMP();
            bmpParser.saveNewBMP(currentPath + "\\" + textOutputFileNameField.getText());
        }
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        bmpParserGUI dialog = new bmpParserGUI();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
